import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import Modal from 'react-native-modal';
import {Colors} from '../global/Colors';

export default class DetailModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MoreDsc: 2,
      MorePairing: 2,
    };
  }
  ChangeLine(state, nameState) {
    nameState === null
      ? this.setState({[state]: 2})
      : this.setState({[state]: null});
  }
  close() {
    const {onClose} = this.props;
    this.setState({MoreDsc: 2, MorePairing: 2}, onClose);
  }
  render() {
    const {isModal, Data} = this.props;
    const {MoreDsc, MorePairing} = this.state;
    return (
      <Modal
        animationIn="fadeIn"
        animationInTiming={100}
        animationOutTiming={100}
        animationOut="fadeOut"
        useNativeDriver
        onBackButtonPress={() => this.close()}
        onBackdropPress={() => this.close()}
        isVisible={isModal}>
        <View>
          <TouchableOpacity onPress={() => this.close()} style={styles.Close}>
            <Text style={styles.CloseText}>CLOSE</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={styles.ModalWrapper}>
          <View style={styles.Detail}>
            <View style={styles.TextWrapper}>
              <Text style={styles.Name}>{Data.name}</Text>
              <Text style={styles.DetailText}>{Data.tagline}</Text>
              <Text style={styles.DetailText}>{Data.abv}</Text>
              <Text
                onPress={() => this.ChangeLine('MoreDsc', MoreDsc)}
                numberOfLines={MoreDsc}
                style={styles.DetailText}>
                {Data.description}
              </Text>
              <Text
                onPress={() => this.ChangeLine('MorePairing', MorePairing)}
                numberOfLines={MorePairing}
                style={styles.DetailText}>
                {Data.food_pairing}
              </Text>
            </View>
            <View style={styles.ImgWrapper}>
              <Image
                resizeMode="contain"
                style={styles.ImgModal}
                source={{uri: Data.image_url}}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.close();
              this.props.AddTocart(Data);
            }}
            style={styles.Btn}>
            <Text style={styles.BtnText}>ADD TO CART</Text>
          </TouchableOpacity>
        </ScrollView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  DetailText: {
    color: Colors.Light,
    opacity: 0.7,
    fontSize: 16,
    marginBottom: 5,
  },
  Name: {
    color: Colors.Light,
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  TextWrapper: {
    flex: 1,
  },
  Btn: {
    borderRadius: 15,
    backgroundColor: Colors.Light,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    alignSelf: 'flex-end',
    marginBottom: 20,
  },
  ImgWrapper: {
    borderRadius: 15,
    backgroundColor: Colors.Light,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
  },
  BtnText: {
    fontSize: 16,
  },
  ImgModal: {
    height: 90,
    width: 90,
  },
  ModalWrapper: {
    backgroundColor: Colors.Dark,
    borderRadius: 15,
    marginHorizontal: 15,
    flexGrow: 0,
    padding: 10,
    zIndex: 1,
    marginTop: 30,
  },
  CloseText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  Detail: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    paddingTop: 10,
  },
  Close: {
    backgroundColor: Colors.Red,
    borderRadius: 15,
    height: 35,
    width: 75,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 10,
    top: -10,
    zIndex: 99,
  },
});
