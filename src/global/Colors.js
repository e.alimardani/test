export const Colors = {
  Red: '#db0000',
  Dark: '#2e2e2d',
  Yellow: '#f7b600',
  Gray: '#8d8d8c',
  Light: '#fefefd',
};
