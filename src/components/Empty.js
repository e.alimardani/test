import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {Colors} from '../global/Colors';

const Empty = () => {
  return <Text style={styles.Text}>No Product</Text>;
};
export default Empty;

const styles = StyleSheet.create({
  Text: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 15,
    color: Colors.Gray,
  },
});
