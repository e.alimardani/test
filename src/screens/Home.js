import React, {Component} from 'react';
import {
  View,
  StatusBar,
  ToastAndroid,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Tab, Tabs, Icon} from 'native-base';
import Option from '../components/Option';
import {Colors} from '../global/Colors';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import Empty from '../components/Empty';

const URL = 'https://api.punkapi.com/v2/beers';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: [],
      isLoading: true,
      isModal: false,
      BasketData: [],
      amount: 1,
    };
  }
  async componentDidMount() {
    await this.GetData();
    let init = await AsyncStorage.getItem('Basket');
    if (init) {
      await this.setState({BasketData: JSON.parse(init)});
    }
  }
  renderBasket = ({index, item}) => {
    return (
      <View style={styles.Detail}>
        <View style={styles.ImgWrapper}>
          <Image
            resizeMode="contain"
            style={styles.Img}
            source={{uri: item.image_url}}
          />
        </View>
        <Text style={styles.Name}>{item.name}</Text>
        <View style={styles.Numbers}>
          <TouchableOpacity style={styles.Minus}>
            <Icon style={styles.MinusIcon} type="FontAwesome" name="minus" />
          </TouchableOpacity>
          <Text style={styles.Amount}>{this.state.amount}</Text>
          <TouchableOpacity style={styles.Pluse}>
            <Icon style={styles.PluseIcon} type="FontAwesome" name="plus" />
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => this.Remove(index)}>
          <Icon style={styles.TrashIcon} type="FontAwesome5" name="trash-alt" />
        </TouchableOpacity>
      </View>
    );
  };
  GetData = async () => {
    try {
      let response = await fetch(URL, {
        method: 'GET',
      });
      const json = await response.json();
      this.setState({isLoading: false});
      if (response.status === 200) {
        this.setState({Data: json});
      } else {
        ToastAndroid.show('error occurs', ToastAndroid.SHORT);
      }
    } catch (error) {
      this.setState({isLoading: false});
      ToastAndroid.show('error occurs', ToastAndroid.SHORT);
    }
  };
  async Remove(index) {
    let Basket = this.state.BasketData;
    Basket.splice(index, 1);
    await this.setState({BasketData: Basket});
    await AsyncStorage.setItem('Basket', JSON.stringify(this.state.BasketData));
  }
  AddToCart(data) {
    const existingItem = this.state.BasketData.find(
      (item) => item.id === data.id,
    );
    if (existingItem) {
      ToastAndroid.show('This item already exist', ToastAndroid.SHORT);
    } else {
      this.setState({
        BasketData: [...this.state.BasketData, data],
        isModal: true,
      });
      AsyncStorage.setItem('Basket', JSON.stringify(this.state.BasketData));
    }
  }
  render() {
    const {Data, isLoading, isModal, BasketData} = this.state;
    return (
      <SafeAreaView style={styles.Container}>
        <StatusBar hidden />
        <View style={styles.Title}>
          <Text style={styles.TitleText}>Demo App</Text>
        </View>
        <Tabs
          tabContainerStyle={styles.TabContainer}
          tabBarUnderlineStyle={styles.TabBarUnderline}
          tabBarInactiveTextColor={Colors.Gray}
          tabBarActiveTextColor={Colors.Light}>
          <Tab
            activeTabStyle={styles.TabStyle}
            textStyle={styles.TextTab}
            tabStyle={styles.TabStyle}
            activeTextStyle={styles.ActiveText}
            heading="All">
            <Option
              Refresh={() => this.GetData()}
              Data={Data}
              isLoading={isLoading}
              AddToCart={(cart) => this.AddToCart(cart)}
            />
          </Tab>
          <Tab
            activeTabStyle={styles.TabStyle}
            activeTextStyle={styles.ActiveText}
            tabStyle={styles.TabStyle}
            textStyle={styles.TextTab}
            heading="PIZZA">
            <Option
              Refresh={() => this.GetData()}
              Data={Data}
              isLoading={isLoading}
              AddToCart={(cart) => this.AddToCart(cart)}
            />
          </Tab>
          <Tab
            activeTextStyle={styles.ActiveText}
            activeTabStyle={styles.TabStyle}
            tabStyle={styles.TabStyle}
            textStyle={styles.TextTab}
            heading="STEAK">
            <Option
              Refresh={() => this.GetData()}
              Data={Data}
              isLoading={isLoading}
              AddToCart={(cart) => this.AddToCart(cart)}
            />
          </Tab>
        </Tabs>
        <Modal
          useNativeDriver
          animationIn="slideInUp"
          animationOut="slideOutDown"
          style={styles.Modal}
          onBackButtonPress={() => this.setState({isModal: false})}
          onBackdropPress={() => this.setState({isModal: false})}
          swipeDirection="down"
          onSwipeComplete={() => this.setState({isModal: false})}
          isVisible={isModal}>
          <View style={styles.ModalWrapper}>
            <View style={styles.Border} />
            <View style={styles.TitleModal}>
              <Icon
                style={styles.TitleModalIcon}
                type="FontAwesome"
                name="shopping-basket"
              />
              <Text style={styles.TitleModalText}>Shopping Cart</Text>
            </View>
            <FlatList
              keyExtractor={(item) => item.id.toString()}
              data={BasketData}
              ListEmptyComponent={<Empty />}
              renderItem={this.renderBasket}
            />
          </View>
        </Modal>
        <TouchableOpacity
          onPress={() => this.setState({isModal: true})}
          style={styles.Basket}>
          <View style={styles.Border} />
          <View style={styles.TitleModal}>
            <Icon
              style={styles.TitleModalIcon}
              type="FontAwesome"
              name="shopping-basket"
            />
            <Text style={styles.TitleModalText}>Shopping Cart</Text>
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  Title: {
    backgroundColor: Colors.Red,
    paddingVertical: 15,
  },
  TrashIcon: {
    color: Colors.Red,
    opacity: 0.7,
  },
  Basket: {
    backgroundColor: Colors.Dark,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 10,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
  TipsText: {
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    flex: 1,
  },
  Tips: {
    marginHorizontal: 20,
    marginTop: 30,
  },
  TipsTitle: {
    color: Colors.Gray,
    fontSize: 16,
    marginBottom: 5,
  },
  TipsItem: {
    backgroundColor: Colors.Yellow,
    borderRadius: 20,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  TipsBorder: {
    borderLeftColor: Colors.Dark,
    borderWidth: 1,
    height: 50,
  },
  Name: {
    color: Colors.Light,
    fontWeight: 'bold',
    fontSize: 18,
    flex: 1,
    marginLeft: 10,
  },
  Amount: {
    marginHorizontal: 15,
    color: Colors.Light,
    fontWeight: 'bold',
    fontSize: 18,
  },
  Numbers: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  Pluse: {
    backgroundColor: Colors.Yellow,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    height: 20,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Minus: {
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
    height: 20,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.Gray,
    borderWidth: 1,
  },
  PluseIcon: {
    fontSize: 14,
  },
  MinusIcon: {
    fontSize: 14,
    color: Colors.Gray,
  },
  TitleText: {
    color: Colors.Dark,
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  ImgWrapper: {
    borderRadius: 10,
    backgroundColor: Colors.Light,
    padding: 10,
    width: 50,
    height: 80,
  },
  Img: {
    width: undefined,
    height: undefined,
    flex: 1,
  },
  Detail: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  Container: {
    flex: 1,
  },
  Border: {
    borderTopWidth: 2,
    borderRadius: 10,
    borderTopColor: Colors.Gray,
    width: 30,
    alignSelf: 'center',
  },
  TitleModal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
  },
  TitleModalIcon: {
    fontSize: 14,
    color: Colors.Gray,
  },
  TitleModalText: {
    color: Colors.Gray,
    fontWeight: 'bold',
    marginLeft: 10,
    fontSize: 16,
  },
  TabStyle: {
    backgroundColor: Colors.Dark,
  },
  ActiveText: {
    fontWeight: 'bold',
    color: Colors.Light,
    fontSize: 18,
  },
  ModalWrapper: {
    flex: 1,
    backgroundColor: Colors.Dark,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 15,
  },
  Modal: {
    margin: 0,
    marginTop: '20%',
  },
  TabBarUnderline: {backgroundColor: 'transparent'},
  TextTab: {
    fontSize: 18,
    color: Colors.Gray,
    fontWeight: 'bold',
  },
  TabContainer: {
    elevation: 0,
    backgroundColor: Colors.Dark,
    paddingHorizontal: '10%',
  },
});
