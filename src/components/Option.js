import React, {Component} from 'react';
import {
  FlatList,
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import {Colors} from '../global/Colors';
import Empty from './Empty';
import DetailModal from './DetailModal';

export default class Option extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal: false,
      DetailProduct: [],
      total: [],
    };
  }
  renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => this.setState({isModal: true, DetailProduct: item})}
        style={styles.Items}>
        <View style={styles.ItemImg}>
          <Image
            resizeMode="contain"
            style={styles.Img}
            source={{uri: item.image_url}}
          />
        </View>
        <Text numberOfLines={1} style={styles.ItemText}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  async SaveItem(Data) {
    this.props.AddToCart(Data);
  }
  render() {
    const {Data, isLoading, Refresh} = this.props;
    const {isModal, DetailProduct} = this.state;
    return (
      <View>
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={Data}
          onRefresh={Refresh}
          refreshing={isLoading}
          ListEmptyComponent={!isLoading ? <Empty /> : false}
          numColumns={3}
          columnWrapperStyle={styles.ColumnWrapper}
          renderItem={this.renderItem}
        />
        <DetailModal
          isModal={isModal}
          onClose={() => this.setState({isModal: false})}
          Data={DetailProduct}
          AddTocart={(data) => this.SaveItem(data)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ColumnWrapper: {
    justifyContent: 'space-between',
    padding: 5,
  },
  Img: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  Items: {
    flex: 1,
    height: 150,
    margin: 10,
  },
  ItemText: {
    textAlign: 'center',
    fontSize: 16,
  },
  ItemImg: {
    padding: 10,
    borderColor: Colors.Gray,
    borderWidth: 1.5,
    borderRadius: 10,
    flex: 1,
  },
});
